﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;

public class ClientSocket
{
    public ClientSocket(string ipAddress)
	{
        sck = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        if (ipAddress == "localHost")
        {
            localEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1234);
        }

        else
        {
            try
            {
                localEndPoint = new IPEndPoint(IPAddress.Parse(ipAddress), 1234);
            }
            catch
            {
                Console.WriteLine("Parsing error!");
            }
        }
    }

    public void Connect()
    {
        try
        {
            sck.Connect(localEndPoint);
            Console.WriteLine("Connection successfull!");
        }
        catch
        {
            Console.WriteLine("Connection Error! Attempting to connect...");
            Thread.Sleep(400);
            Connect();
        }
    }

    public void Send(string data)
    {
        byte[] byteData = Encoding.ASCII.GetBytes(data);

        sck.Send(byteData);
    }

    public string Receive()
    {
        byte[] Buffer;

        Buffer = new byte[sck.SendBufferSize];
        int bytesRead = sck.Receive(Buffer);

        /*
         * Reza a lenda que quando uma conexao e encerrada,
         * accepted.Receive(buffer) retorna '0'.
         */

        if (bytesRead == 0)
        {
            Console.WriteLine("Connection closed!");
            return null;
        }

        else
        {
            byte[] formatted = new byte[bytesRead];

            for (int i = 0; i < bytesRead; i++)
            {
                formatted[i] = Buffer[i];
            }

            string strData = Encoding.ASCII.GetString(formatted);

            return strData;
        }
    }

    private Socket sck;
    private IPEndPoint localEndPoint;
}
