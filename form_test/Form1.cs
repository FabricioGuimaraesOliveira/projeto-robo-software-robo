﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ControlBeanExDll;
using TcpserverExDll;
using System.Threading;
using System.Globalization;
using System.IO.Ports;
using System.IO;

namespace form_test
{
    public partial class Form1 : Form
    {
        ControlBeanEx z_arm;
        ClientSocket sock = new ClientSocket("localHost");
        SerialPort port;

        float x_position, y_position, z_position, angle;

        //Posição dos Fiduciais na posição zero
        const float x_fiducial_position_0= 179.93f, y_fiducial_position_0= 8.11f, z_fiducial_position_0=-70,angle_fiducial_position_0 = 109.72f;

        //Posição dos Fiduciais na posição um
        const float x_fiducial_position_1 = 296.93f, y_fiducial_position_1 = 163.11f, z_fiducial_position_1= -70, angle_fiducial_position_1 = 109.72f;

  

        const float speed = 150;
        const float speed_place = 10;
        const bool open = false;

        bool isStopEnabled = false;

        // const float x_place = 312.10f, y_place = 28.10f,
        //angle_place = 289.20f; //melhor encaixe
        const float x_place = 313.30f, y_place = 31.60f,
                angle_place = 109.40f; //melhor encaixe

        float x_shift_place = 0, y_shift_place = 0,
                angle_shift_place =0; //shift encaixe

        const string comPort = "COM13";


        public Form1()
        {
            InitializeComponent();
            label1.Enabled = false;
            label2.Enabled = false;
            label3.Enabled = false;
            label4.Enabled = false;
            textBox1.Enabled = false;
            textBox2.Enabled = false;
            textBox3.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;

            this.FormClosed += new FormClosedEventHandler(Form1_FormClosed);

        }

        private void get_textBox_values()
        {
            x_position = float.Parse(textBox1.Text, System.Globalization.CultureInfo.InvariantCulture);
            y_position = float.Parse(textBox2.Text, System.Globalization.CultureInfo.InvariantCulture);
            z_position = float.Parse(textBox3.Text, System.Globalization.CultureInfo.InvariantCulture);
            angle = float.Parse(textBox4.Text, System.Globalization.CultureInfo.InvariantCulture);
        }

        private void set_textBox_values()
        {
            z_arm.get_scara_param();

            x_position = z_arm.x;
            y_position = z_arm.y;
            z_position = z_arm.z;
            angle = z_arm.rotation;

            textBox1.Text = x_position.ToString("0.00", CultureInfo.InvariantCulture);
            textBox2.Text = y_position.ToString("0.00", CultureInfo.InvariantCulture);
            textBox3.Text = z_position.ToString("0.00", CultureInfo.InvariantCulture);
            textBox4.Text = angle.ToString("0.00", CultureInfo.InvariantCulture);
        }

        private void button1_Click(object sender, EventArgs e)
        {


            int ret;
            int error_code;
            float z_position;

            TcpserverEx.net_port_initial();

            //O argumento do método get_robot é 111 (e não 251).
            z_arm = TcpserverEx.get_robot(111);

            while (true)
            {
                if (z_arm.is_connected())
                {
                    break;
                }
                Thread.Sleep(100);
            }

            if (z_arm.is_connected())
            {
                ret = z_arm.initial(1, 210);
                if (ret == 1)
                {
                    z_arm.unlock_position();
                    z_arm.set_arm_length(200, 200);
                }
            }


            set_textBox_values();

            /*
            Thread receiveData = new Thread(enableReceive);
            receiveData.Start();
            

            
            if (port == null)
            {
                port = new SerialPort(comPort, 9600);
                port.DtrEnable = true;
                port.Open();

                port.DataReceived += port_DataReceived;
            }

            */
            //sock.Connect();
           


            ////error_code = z_arm.single_joint_move(1, -10, 15);

            ////funcao xyz_move se movimenta x unidades a partir do ponto atual.
            ////error_code = z_arm.xyz_move(1, -20, 10);

            ////error_code = z_arm.set_position_move(200, 120, -170, 100, 150, 1, 1, 1);

            ///*
            //while (true) {
            //    //z_position = float.Parse(textBox1.Text);
            //    while (z_arm.set_position_move(75, -120, -20, 100, 100, 1, 1, 1) == 0) {}
            //    while (z_arm.set_position_move(280, 150, -80, 100, 100, 1, 1, 1) == 0) { }

            //}
            //*/
            ///*z_position = float.Parse(textBox1.Text);
            //error_code = z_arm.set_position_move(75, -120, z_position, 100, 100, 1, 1, 1);

            //MessageBox.Show(error_code.ToString());*/
            button1.Enabled = false;

            //label1.Enabled = true;
            label2.Enabled = true;
            label3.Enabled = true;
            textBox1.Enabled = true;
            textBox2.Enabled = true;
            textBox3.Enabled = true;
            button2.Enabled = true;
            label4.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
        }

        void enableReceive()
        {
            if (port == null)
            {
                port = new SerialPort(comPort, 9600);
                port.DtrEnable = true;
                port.Open();

                port.DataReceived += port_DataReceived;
            }
        }

        void f_Load(object sender, EventArgs e)
        { }

        private void button2_Click(object sender, EventArgs e)
        {
            int error_code;

            get_textBox_values();
            error_code = z_arm.set_position_move(x_position, y_position, z_position, angle, speed, 1, 1, 1);

            MessageBox.Show(error_code.ToString());
            //error_code = z_arm.set_efg_state(8, 0);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            bool error_code;

            error_code = z_arm.set_digital_out(0, false);
            MessageBox.Show(error_code.ToString());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            bool error_code;

            error_code = z_arm.set_digital_out(0, true);
            MessageBox.Show(error_code.ToString());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            get_textBox_values();

            x_position += 0.1f;

            textBox1.Text = x_position.ToString("0.00", CultureInfo.InvariantCulture);

            z_arm.set_position_move(x_position, y_position, z_position, angle, 10, 1, 1, 1);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            get_textBox_values();

            y_position += 0.1f;

            textBox2.Text = y_position.ToString("0.00", CultureInfo.InvariantCulture);

            z_arm.set_position_move(x_position, y_position, z_position, angle, 10, 1, 1, 1);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            get_textBox_values();

            z_position += 0.1f;

            textBox3.Text = z_position.ToString("0.00", CultureInfo.InvariantCulture);

            z_arm.set_position_move(x_position, y_position, z_position, angle, 10, 1, 1, 1);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            get_textBox_values();

            x_position -= 0.1f;

            textBox1.Text = x_position.ToString("0.00", CultureInfo.InvariantCulture);

            z_arm.set_position_move(x_position, y_position, z_position, angle, 10, 1, 1, 1);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            get_textBox_values();
            angle -= 0.1f;

            textBox4.Text = angle.ToString("0.00", CultureInfo.InvariantCulture);

            z_arm.set_position_move(x_position, y_position, z_position, angle, 10, 1, 1, 1);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            get_textBox_values();
            x_position++;
            textBox1.Text = x_position.ToString("0.00", CultureInfo.InvariantCulture);
            z_arm.set_position_move(x_position, y_position, z_position, angle, 10, 1, 1, 1);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            get_textBox_values();
            x_position--;
            textBox1.Text = x_position.ToString("0.00", CultureInfo.InvariantCulture);
            z_arm.set_position_move(x_position, y_position, z_position, angle, 10, 1, 1, 1);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            get_textBox_values();
            y_position++;
            textBox2.Text = y_position.ToString("0.00", CultureInfo.InvariantCulture);
            z_arm.set_position_move(x_position, y_position, z_position, angle, 10, 1, 1, 1);
        }

        private void button16_Click(object sender, EventArgs e)
        {
            get_textBox_values();
            y_position--;
            textBox2.Text = y_position.ToString("0.00", CultureInfo.InvariantCulture);
            z_arm.set_position_move(x_position, y_position, z_position, angle, 10, 1, 1, 1);
        }

        private void button17_Click(object sender, EventArgs e)
        {
            get_textBox_values();
            z_position++;
            textBox3.Text = z_position.ToString("0.00", CultureInfo.InvariantCulture);
            z_arm.set_position_move(x_position, y_position, z_position, angle, 10, 1, 1, 1);
        }

        private void button18_Click(object sender, EventArgs e)
        {
            get_textBox_values();
            z_position--;
            textBox3.Text = z_position.ToString("0.00", CultureInfo.InvariantCulture);
            z_arm.set_position_move(x_position, y_position, z_position, angle, 10, 1, 1, 1);
        }

        private void button19_Click(object sender, EventArgs e)
        {
            get_textBox_values();
            angle++;
            textBox4.Text = angle.ToString("0.00", CultureInfo.InvariantCulture);
            z_arm.set_position_move(x_position, y_position, z_position, angle, 10, 1, 1, 1);
        }

        private void button20_Click(object sender, EventArgs e)
        {
            get_textBox_values();
            angle--;
            textBox4.Text = angle.ToString("0.00", CultureInfo.InvariantCulture);
            z_arm.set_position_move(x_position, y_position, z_position, angle, 10, 1, 1, 1);
        }

        private void button22_Click(object sender, EventArgs e)
        {
            open_finger();
        }

        private void button21_Click(object sender, EventArgs e)
        {
            close_finger();
        }

        private void show_place_position(float x_position, float y_position, float z_position, float rotation)
        {
            textBox1.Text = x_position.ToString("0.00", CultureInfo.InvariantCulture);
            textBox2.Text = y_position.ToString("0.00", CultureInfo.InvariantCulture);
            textBox3.Text = z_position.ToString("0.00", CultureInfo.InvariantCulture);
            textBox4.Text = rotation.ToString("0.00", CultureInfo.InvariantCulture);
        }

        private void move_to_position(float x, float y, float z, float angle)
        {
            int status;
            status = z_arm.set_position_move(x, y, z, angle, speed, 1, 1, 1);
            Console.WriteLine(status);
            while (!z_arm.is_robot_goto_target()) {}
            show_place_position(x, y, z, angle);
        }

        private void move_to_position(float x, float y, float z, float angle, float varSpeed)
        {
            int status;
            status = z_arm.set_position_move(x, y, z, angle, varSpeed, 1, 1, 1);
            Console.WriteLine(status);
            while (!z_arm.is_robot_goto_target()) { }
            show_place_position(x, y, z, angle);
        }

        private void open_finger()
        {
            z_arm.set_digital_out(2, open);
        }

        private void close_finger()
        {
            z_arm.set_digital_out(2, !open);
        }

        private void pick_single_first()
        {
            //Initial position
            open_finger();

            ////move_to_position(171.11f, -283.02f, -10.00f, 289.52f);

            //////Move to pick position
            //move_to_position(26.00f, -278.00f, -50.00f, 108.00f);

            //////Move down
            //move_to_position(26.00f, -278.00f, -90.60f, 108.00f, 50.00f);

            //////Pick component
            //Thread.Sleep(400);
            //close_finger();

            //////Move up
            //move_to_position(26.00f, -278.00f, -40.00f, 110.00f, 50.00f);


            //Move to pick position
            move_to_position(21.52f, -187.64f, -50.00f, 290.00f, 100.00f);

            //Move down
            move_to_position(21.52f, -187.64f, -91.00f, 290.00f, 50.00f);

            //Pick component
            Thread.Sleep(400);
            close_finger();

            //Move up
            move_to_position(21.52f, -187.64f, -40.00f, 290.00f, 50.00f);

            //Trespass sensor
            move_to_position(73.67f, -100.20f, -40.00f, angle_place);

            //Move to place position
            //move_to_position(x_place, y_place, -40.00f, angle_place);
            move_to_position(180.93f, 18.11f, -40f, 109.72f);

        }

        private void pick_single_second()
        {
            //Initial position
            z_arm.set_digital_out(2, false);
            move_to_position(-106.54f, -285.94f, -48.25f, 290.10f);
            move_to_position(171.11f, -283.02f, -48.25f, 288.82f);

            //Move to pick position
            move_to_position(-106.54f, -285.94f, -50.25f, 290.10f);

            //Move down
            move_to_position(-106.54f, -285.94f, -148.25f, 290.10f);

            //Pick component
            z_arm.set_digital_out(2, true);

            //Move up
            move_to_position(-106.54f, -285.94f, -48.25f, 290.10f);
            /*
            //Move to place position
            move_to_position(171.11f, -283.02f, -48.25f, 288.82f);

            //Move down
            move_to_position(171.11f, -283.02f, -205.25f, 288.82f);
            z_arm.set_digital_out(2, false);

            //Move up
            move_to_position(171.11f, -283.02f, -50.25f, 288.82f);

            //Return home
            move_to_position(171.11f, -283.02f, -50.25f, 288.82f);
        */
        }

        private void pick_single_third()
        {
            //Open finger
            z_arm.set_digital_out(2, false);

            //Initial position
            //move_to_position(171.11f, -283.02f, -48.25f, 288.82f);

            /*
            //Move to pick position
            move_to_position(-63.89f, -283.02f, -50.25f, 288.82f);

            //Move down
            move_to_position(-63.89f, -283.02f, -148.25f, 288.82f);

            //Pick component
            z_arm.set_digital_out(2, true);

            //Move up
            move_to_position(-63.89f, -283.02f, -48.25f, 288.82f);

            //Move to place position
            move_to_position(171.11f, -283.02f, -48.25f, 288.82f);

            /*
            //Move down
            move_to_position(171.11f, -283.02f, -205.25f, 288.82f);
            z_arm.set_digital_out(2, false);

            //Move up
            move_to_position(171.11f, -283.02f, -50.25f, 288.82f);

            //Return home
            move_to_position(171.11f, -283.02f, -50.25f, 288.82f);*/
        }

        private void pick_first_Click(object sender, EventArgs e)
        {
            pick_single_first();
        }

        private void pick_second_Click(object sender, EventArgs e)
        {
            pick_single_second();
        }

        private void pick_third_Click(object sender, EventArgs e)
        {
        pick_single_third();

            

        }

        private void pick_all_Click(object sender, EventArgs e)
        {
            pick_single_first();
            pick_single_second();
            pick_single_third();
        }

        private void button23_Click(object sender, EventArgs e)
        {

            z_arm.set_digital_out(2, true); 
            move_to_position(-142.19f, -284.05f, -50.00f, 289.44f);
            z_arm.set_digital_out(2, false);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void waitBoard()
        {
            string response;

            while(true)
            {
                sock.Send("c01");
                response = sock.Receive();

                Console.WriteLine(response);

                if (response == "sim")
                {
                    Console.WriteLine("A placa chegou, minha filha!");
                    break;
                }
                else
                {
                    Thread.Sleep(50);
                }
            }

            //Thread.Sleep(1000);
            
        }

        private void waitBoardStop()
        {
            string response;

            while (true)
            {
                sock.Send("c01");
                response = sock.Receive();

                Console.WriteLine(response);

                if (response == "sim")
                {
                    Console.WriteLine("A placa chegou, minha filha!");
                    break;
                }
                else
                {
                    Thread.Sleep(50);
                }
            }

            //Thread.Sleep(1000);

        }

        private void button25_Click(object sender, EventArgs e)
        {

            /*pick_single_first();

            //Move to first fiducial
            move_to_position(180.93f, 18.11f, -60.77f, 109.72f);*/

            while (true)
            {
                pick_single_first();
                waitBoard();

                //get_fiducials();
                place_component();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (port != null && port.IsOpen)
            {
                port.Close();
            }
        }

        private void button26_Click(object sender, EventArgs e)
        {

            pick_single_first();

            while (true)
            {

                if (isStopEnabled == true)
                {
                    Thread.Sleep(10);
                    //get_fiducials();
                    mechanical_place_component();
                    pick_single_first();

                    isStopEnabled = false;
                    Thread.Sleep(10000);

                    Console.WriteLine("Encaixando...");
                }

                //get_fiducials();
            }
        }

        private void place_component()
        {
            Console.WriteLine(x_place.ToString() + ": " + y_place.ToString() + ":" + angle_place.ToString());
            Console.WriteLine(x_shift_place.ToString() + ": " + y_shift_place.ToString() + ":" + angle_shift_place.ToString());
            //Move down
            move_to_position(x_shift_place, y_shift_place, -40.00f, angle_shift_place, speed);
            //Thread.Sleep(200);

            move_to_position(x_shift_place, y_shift_place, -85.00f, angle_shift_place, speed);
            //Move down
            move_to_position(x_shift_place, y_shift_place, -89.00f, angle_shift_place, speed_place);

            //Place component



            //move_to_position(x_place, y_place, -40.00f, angle_place, speed);
            //Thread.Sleep(200);

            //move_to_position(x_place, y_place, -80.00f, angle_place, speed);
            ////Move down
            //move_to_position(x_place, y_place, -92.00f, angle_place, speed_place);



            Thread.Sleep(100);
            open_finger();

            ////Move up
            move_to_position(x_place, y_place, -50.00f, angle_place);

            ////Return home
            move_to_position(-142.19f, -284.05f, -50.00f, 289.44f);


            //Thread.Sleep(100);

        }

        private void mechanical_place_component()
        {
            Console.WriteLine(x_place.ToString() + ": " + y_place.ToString() + ":" + angle_place.ToString());
            
            //Move down
            move_to_position(x_place, y_place, -40.00f, angle_place, speed);
            move_to_position(x_place, y_place, -80.00f, angle_place, speed);
            
            //Move down
            move_to_position(x_place, y_place, -87.60f, angle_place, speed_place);

            Thread.Sleep(400);
            open_finger();

            ////Move up
            move_to_position(x_place, y_place, -50.00f, angle_place);

            ////Return home
            move_to_position(-142.19f, -284.05f, -50.00f, 289.44f);

        }

        private void button24_Click(object sender, EventArgs e)
        {
            place_component();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            get_textBox_values();

            y_position -= 0.1f;

            textBox2.Text = y_position.ToString("0.00", CultureInfo.InvariantCulture);

            z_arm.set_position_move(x_position, y_position, z_position, angle, 10, 1, 1, 1);
        }

        private void get_fiducials()
        {

            /*String X;
             * /
            */

            //Move to first fiducial
            move_to_position(x_fiducial_position_0, y_fiducial_position_0, -30, angle_fiducial_position_0, speed);
            move_to_position(x_fiducial_position_0, y_fiducial_position_0, z_fiducial_position_0, angle_fiducial_position_0, speed);
            Thread.Sleep(100);

            sock.Send("GF0");
            String response = sock.Receive();
            Console.WriteLine("Lendo Fiducial 0  " + response);

            Thread.Sleep(100);
            move_to_position(x_fiducial_position_0, y_fiducial_position_0,-60, angle_fiducial_position_0, speed);

            //Move to second fiducial
            move_to_position(x_fiducial_position_1, y_fiducial_position_1, -60, angle_fiducial_position_1, speed);
            move_to_position(x_fiducial_position_1, y_fiducial_position_1, z_fiducial_position_1, angle_fiducial_position_1, speed);
            Thread.Sleep(100);
            sock.Send("GF1");
            response = sock.Receive();
            Console.WriteLine("Lendo Fiducial 1  " + response);
            Thread.Sleep(400);
            move_to_position(x_fiducial_position_1, y_fiducial_position_1, -60, angle_fiducial_position_1, speed);

            // Get Board Shift

            sock.Send("GBS");
            response = sock.Receive();
            Console.WriteLine("Shift Board " + response);

            
            //Console.WriteLine("Shift Board " + response);
            
            response = response.Replace('[', ' ');
            response = response.Replace(']', ' ');
            response = response.Replace(',', ';');
            response = response.Replace('.', ',');
            String[] results = response.Split(';');

            try
            {
                Console.WriteLine("Shift Position " + results[0] + " : " + results[1] + " : " + results[2]);


                Console.WriteLine("Orig  Position " + float.Parse(results[0]).ToString() + " : " + float.Parse(results[1]).ToString() + " : " + float.Parse(results[2]).ToString());
                x_shift_place = x_place + float.Parse(results[0]);
                y_shift_place = y_place + float.Parse(results[1]);
                angle_shift_place = angle_place + float.Parse(results[2]);
                Console.WriteLine("Shift Position " + x_shift_place + " : " + y_shift_place + " : " + angle_shift_place);
            }
            catch
            {

            }
            



        }

        private void get_fiducials_Treino()
        {

            /*String X;
             * /
            */

            //Move to first fiducial
            move_to_position(x_fiducial_position_0, y_fiducial_position_0, -30, angle_fiducial_position_0, speed);
            move_to_position(x_fiducial_position_0, y_fiducial_position_0, z_fiducial_position_0, angle_fiducial_position_0, speed);
            Thread.Sleep(200);

            sock.Send("T00");
            String response = sock.Receive();
            Console.WriteLine("Lendo Fiducial 0  " + response);

            Thread.Sleep(400);
            move_to_position(x_fiducial_position_0, y_fiducial_position_0, -60, angle_fiducial_position_0, speed);

            //Move to second fiducial
            move_to_position(x_fiducial_position_1, y_fiducial_position_1, -60, angle_fiducial_position_1, speed);
            move_to_position(x_fiducial_position_1, y_fiducial_position_1, z_fiducial_position_1, angle_fiducial_position_1, speed);
            Thread.Sleep(400);
            sock.Send("T01");
            response = sock.Receive();
            Console.WriteLine("Lendo Fiducial 1  " + response);
            Thread.Sleep(400);
            move_to_position(x_fiducial_position_1, y_fiducial_position_1, -40, angle_fiducial_position_1, speed);

  

          
        }


        private void button27_Click(object sender, EventArgs e)
        {
            get_fiducials();
        }

        private void button28_Click(object sender, EventArgs e)
        {
            //Initial position
            open_finger();

            //move_to_position(171.11f, -283.02f, -10.00f, 289.52f);

            //Move to pick position
            move_to_position(26.00f, -278.00f, -50.00f, 108.00f);

            //Move down
            move_to_position(26.00f, -278.00f, -90.60f, 108.00f, 50.00f);

            //Pick component
            Thread.Sleep(400);
            close_finger();

            //Move up
            move_to_position(26.00f, -278.00f, -40.00f, 110.00f, 50.00f);

            /*
            //Trespass sensor
            move_to_position(73.67f, -100.20f, -40.00f, angle_place);

            //Move to place position
            //move_to_position(x_place, y_place, -40.00f, angle_place);
            move_to_position(180.93f, 18.11f, -40f, 109.72f);*/
        }


        private void button29_Click(object sender, EventArgs e)
        {
            //Move down
            move_to_position(x_place, y_place, -40.00f, angle_place, speed);
            move_to_position(x_place, y_place, -80.00f, angle_place, speed);

            //Move down
            move_to_position(x_place, y_place, -83.00f, angle_place, speed_place);

            Thread.Sleep(400);
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void button35_Click(object sender, EventArgs e)
        {
            //get_textBox_values();
            //string[] lines = { "First line", "Second line", "Third line" };
            //// WriteAllLines creates a file, writes a collection of strings to the file,
            //// and then closes the file.  You do NOT need to call Flush() or Close().
            //System.IO.File.WriteAllLines(@"C:\Users\MAPTECHNOLOGY\Documents\ProjetoRobo\ProjetoFinal\C#\C#\ WriteLines.txt", lines);

        }

        private void button36_Click(object sender, EventArgs e)
        {
            pick_single_first();
            mechanical_place_component();
            Thread.Sleep(1000);
        }

        private void button37_Click(object sender, EventArgs e)
        {
            get_fiducials_Treino();
        }

        private void button38_Click(object sender, EventArgs e)
        {
            pick_single_first();
            get_fiducials();
            mechanical_place_component();

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void button30_Click(object sender, EventArgs e)
        {
            //Move to first fiducia 0
            move_to_position(x_fiducial_position_0, y_fiducial_position_0, -60f, angle_fiducial_position_0);
            move_to_position(x_fiducial_position_0, y_fiducial_position_0, z_fiducial_position_0, angle_fiducial_position_0);
            Console.WriteLine("Indo para fiducial 0");

            Thread.Sleep(1200);
        }

        private void button32_Click(object sender, EventArgs e)
        {
            //Move to second fiducial 1 
            move_to_position(x_fiducial_position_1, y_fiducial_position_1, -60f, angle_fiducial_position_1);
            move_to_position(x_fiducial_position_1, y_fiducial_position_1, z_fiducial_position_1, angle_fiducial_position_1);
            Console.WriteLine("Indo para fiducial 1");

            Thread.Sleep(1000);

        }

        private void button31_Click(object sender, EventArgs e)
        {
            sock.Send("T00");
            String response = sock.Receive();

            Console.WriteLine("Treinando o fiducial 0  "+ response);
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button33_Click(object sender, EventArgs e)
        {
            sock.Send("T01");
            String response = sock.Receive();

            Console.WriteLine("Treinando o fiducial 1 " + response);
        }

        private void button34_Click(object sender, EventArgs e)
        {
            mechanical_place_component();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            int error_code;

            get_textBox_values();

            z_position -= 0.1f;

            textBox3.Text = z_position.ToString("0.00", CultureInfo.InvariantCulture);

            error_code = z_arm.set_position_move(x_position, y_position, z_position, angle, 10, 1, 1, 1);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            get_textBox_values();

            angle += 0.1f;
            textBox4.Text = angle.ToString("0.00", CultureInfo.InvariantCulture);
            z_arm.set_position_move(x_position, y_position, z_position, angle, 10, 1, 1, 1);
        }

        private void port_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            string line = port.ReadLine();

            if (Int32.Parse(line) == 0)
            {


                isStopEnabled = true;
                Console.Out.WriteLine("Stop ativado");

                /*
                Thread.Sleep(1000);
                place_component();

                pick_single_first();
                Console.Out.WriteLine("Aguardando placa passar pelo sensor...");

                Thread.Sleep(60000);
                */

                //isStopEnabled = true;

            }
            else if (Int32.Parse(line) == 1)
            {

                Console.Out.WriteLine("Stop desativado...");

            }

            //this.BeginInvoke(new LineReceivedEvent(LineReceived), line);
        }

        private delegate void LineReceivedEvent(string line);
        private void LineReceived(string line)
        {



            if (Int32.Parse(line) == 0)
            {


                Console.Out.WriteLine("Stop ativado");

                /*
                Thread.Sleep(1000);
                place_component();

                pick_single_first();
                Console.Out.WriteLine("Aguardando placa passar pelo sensor...");

                Thread.Sleep(60000);
                */

                //isStopEnabled = true;

            }
            else if (Int32.Parse(line) == 1)
            {

                Console.Out.WriteLine("Stop desativado...");

            }



        }

    }
}